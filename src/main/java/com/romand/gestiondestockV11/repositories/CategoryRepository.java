package com.romand.gestiondestockV11.repositories;

import com.romand.gestiondestockV11.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}
