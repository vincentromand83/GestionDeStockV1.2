package com.romand.gestiondestockV11.dtos;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@Builder
public class CategoryDto {

    private String idCategory;

    @NotEmpty(message = "Code is mandatory")
    private String code;
    @NotEmpty
    private String nom;

/*    @JsonIgnore
    private List<Article> articles;*/

}
