package com.romand.gestiondestockV11.mappers;

import com.romand.gestiondestockV11.dtos.CategoryDto;
import com.romand.gestiondestockV11.entities.Category;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CategoryMapper {

    Category categoryDtoToCategory(CategoryDto categoryDto);

    CategoryDto categoryToCategoryDto(Category category);
}
