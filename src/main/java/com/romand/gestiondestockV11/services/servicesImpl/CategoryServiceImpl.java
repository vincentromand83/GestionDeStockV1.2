package com.romand.gestiondestockV11.services.servicesImpl;

import com.romand.gestiondestockV11.dtos.CategoryDto;
import com.romand.gestiondestockV11.mappers.CategoryMapper;
import com.romand.gestiondestockV11.repositories.CategoryRepository;
import com.romand.gestiondestockV11.services.CategoryService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final CategoryMapper categoryMapper;

    public CategoryServiceImpl(CategoryRepository categoryRepository, CategoryMapper categoryMapper) {
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
    }

    @Override
    public CategoryDto getCategoryById(String idCategory) {
        return null;
    }

    @Override
    public Page<CategoryDto> getAllCategories(int page, int size) {
        return null;
    }

    @Override
    public CategoryDto createCategory(CategoryDto categoryDto) {
        return categoryMapper.categoryToCategoryDto(categoryRepository.save(categoryMapper.categoryDtoToCategory(categoryDto)));
    }

    @Override
    public CategoryDto updateCategory(String idCategory, CategoryDto categoryDto) {
        return null;
    }

    @Override
    public String deleteCategoryById(String idCategory) {
        return null;
    }
}
